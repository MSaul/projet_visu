#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <vector>
#include <SFML/Graphics.hpp>
#include "util.hpp"

class Line {
public:
    Line(const sf::Vector2f &a, const sf::Vector2f &b);
    bool collision(const Line &l, sf::Vector2f &coll);

    sf::Vector2f a;
    sf::Vector2f b;
};

class AABB {
public:
    AABB(const sf::Vector2f &pos, const sf::Vector2f &dim);

    void move(const sf::Vector2f &v);
    void setPos(const sf::Vector2f &p);
    bool collision(const AABB &b) const;

    sf::Vector2f m_pos;
    sf::Vector2f m_dim;
};

class Entity : public Observable {
public:
    Entity(const sf::Vector2f &pos, const sf::Vector2f &dim);
    virtual ~Entity();

    virtual void update(double t);
    virtual void takeCollision(Entity &e, bool fromYou);

    void scale(const sf::Vector2f &s);

    virtual void move(const sf::Vector2f &v, double distMax = 0.0);
    virtual void setPos(const sf::Vector2f &p);
    void setPosMove(const sf::Vector2f &p, double smoothRatio);  // se deplace au plus pres de la position / smooth
    void setCenter(const sf::Vector2f &p);
    void setCenterMove(const sf::Vector2f &p, double smoothRatio); //
    void testDeplacLat(bool b) { m_testDeplacLat = b; }

    void setAngle(double a);   // rad
    void rotate(double a);     // rad

    double getAngle() const;   // rad
    const AABB &getAABB() const;
    sf::Vector2f getCenter() const;
    uint getId();

    // code de notification
    static const uint ROTATE = 0;
    static const uint SET_ANGLE = 1;
    static const uint SET_CENTER = 2;
    static const uint MOVE = 3;
    static const uint TAKE_COLLISION = 4;
    static const uint SCALE = 5;

protected:
    // retourne true si collision
    virtual bool collision(Entity &e);
    bool baseMove(const sf::Vector2f &v);
    void forceSetCenter(const sf::Vector2f &c);

    static std::vector<Entity*> m_entities;

private:
    AABB m_aabb;
    double m_angle;   // rad
    bool m_testDeplacLat;

    static uint idMax;
    uint m_id;
};

class Obstacle : public Entity
{
public:
    Obstacle(const sf::Vector2f &a, const sf::Vector2f &b, uint lvl);
    bool collision(Entity &e, sf::Vector2f &p);

    const Line &getLine() const { return m_line;}

    static uint currentLvl;

protected:
    bool collision(Entity &e);

private:
    Line m_line;
    uint m_lvl;
};

class Pad : public Entity
{
public:
    Pad(const AABB &limit);

    void update(double t);

    virtual void move(const sf::Vector2f &v, double distMax = 0.0);
    virtual void setPos(const sf::Vector2f &p);

    void reset();   // place le pad au milieu de la limite

private:
    AABB m_limit;
    double m_lastReset;
};

class Pads : public sf::NonCopyable
{
public:
    static Pads &get();
    ~Pads();

    Pad *create(const AABB &limit);

    Pad *get(uint id);
    const Pad *get(uint id) const;

    uint size() const;

    const std::vector<Pad*> &getPads() const;
    std::vector<Pad*> &getPads();

private:
    static Pads *instance;

    std::vector<Pad*> m_pads;
};

class Ball : public Entity
{
public:
    Ball(const sf::Vector2f &pos, double speed);

    void update(double t);
    void takeCollision(Entity &e, bool fromYou);

    void reset();

    static const uint RESET = 10;

private:
    void takeCollisionNorm(const Entity &e);
    void takeCollisionPad(const Entity &e);
    void takeCollisionObs(const Obstacle &o);

private:
    double m_speed;
    double M_MIN_SPEED;
    const double M_MAX_SPEED;
    double m_lastSpeedUp;
    double m_lastReset;
};

class Goal : public Entity
{
public:
    Goal(const sf::Vector2f &pos, uint id);
    void takeCollision(Entity &e, bool fromYou);
    uint getId() const;

private:
    uint m_id;
};

#endif
