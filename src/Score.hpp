#ifndef SCORE_HPP
#define SCORE_HPP

#include "util.hpp"

class Score : public sf::NonCopyable
{
public:
    static Score &get();
    static sf::Color getColor(uint id);

    uint getScore(uint id) const;
    void add(uint id, uint s);
    void draw(sf::RenderTarget &rt);


private:
    Score();

private:
    static Score *instance;

    uint m_value[2];
    sf::Text m_text[2];
};

#endif
