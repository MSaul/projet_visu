#include "Detection.hpp"

Detection *Detection::instance(0);

Detection &Detection::get()
{
    if(instance == 0)
        instance = new Detection();
    return *instance;
}

Detection::Detection()
    : m_playerNb(0), m_width(0), m_height(0), m_depthMax(0), m_imgDepth(0),
      m_imgRgb(0)

{
    m_utracker = new nite::UserTracker;
    for (uint i = 0; i < LEN(m_jtype); i++)
        m_jtype[i] = DEFAULT_JOINT;
}

int Detection::openDevice()
{
    openni::Status rc = openni::OpenNI::initialize();
    if (rc != openni::STATUS_OK) {
        printf("Failed to initialize OpenNI\n%s\n", openni::OpenNI::getExtendedError());
        return 1;
    }

    const char* deviceUri = openni::ANY_DEVICE;

    rc = m_device.open(deviceUri);
    if (rc != openni::STATUS_OK) {
        printf("Failed to open device\n%s\n", openni::OpenNI::getExtendedError());
        return 1;
    }

    // stream temporarire pour recupérer le depth max
    {
        openni::VideoStream stream;
        stream.create(m_device, openni::SENSOR_DEPTH);
        stream.start();
        m_depthMax = stream.getMaxPixelValue();
        stream.stop();
    }

    m_streamRgb.create(m_device, openni::SENSOR_COLOR);
    m_streamRgb.start();

    nite::NiTE::initialize();

    if (m_utracker->create(&m_device) != nite::STATUS_OK) {
        return 1;
    }

    return 0;
}

int Detection::getPlayerNb() const
{
    return m_playerNb;
}

int Detection::getDepthPlayer (int id)
{
    return m_depths[id];
}

void Detection::computeNextPos()
{
    nite::UserTrackerFrameRef userTrackerFrame;
    openni::VideoFrameRef depthFrame, colorFrame;

    // COLOR
    m_streamRgb.readFrame(&colorFrame);
    const void* buf = colorFrame.getData();
    if (!m_imgRgb) {
        m_imgRgb = new rgb_pixel_t[colorFrame.getHeight()*colorFrame.getWidth()];
    }
    memcpy(m_imgRgb, colorFrame.getData(), colorFrame.getDataSize());


    // DEPTH
    nite::Status rc = m_utracker->readFrame(&userTrackerFrame);
    if (rc != nite::STATUS_OK) {
        printf("GetNextData failed\n");
        return;
    }
    depthFrame = userTrackerFrame.getDepthFrame();
    if (!m_imgDepth) {
        m_imgDepth = new depth_pixel_t[depthFrame.getWidth()*depthFrame.getHeight()];
        m_width = depthFrame.getWidth();
        m_height = depthFrame.getHeight();
    }
    memcpy(m_imgDepth, depthFrame.getData(), depthFrame.getDataSize());

    const nite::UserMap& userLabels = userTrackerFrame.getUserMap();

    const nite::Array<nite::UserData>& users = userTrackerFrame.getUsers();
    for (int i = 0; i < users.getSize(); ++i) {
        const nite::UserData& user = users[i];
        if (user.isNew()) {
            printf("user %d new\n", user.getId());
            m_utracker->startSkeletonTracking(user.getId());
            m_playerNb++;
        }
        else if (user.isLost()) {
            printf("user %d : lost\n", user.getId());
            m_playerNb--;
        }
        else {
            if (user.getSkeleton().getState() == nite::SKELETON_TRACKED) {
                int id = user.getId();

                m_pose[id].setSkeleton(user.getSkeleton(), *this);

                // pos pad
                nite::Point3f p = user.getSkeleton().getJoint(m_jtype[id]).getPosition();
                m_utracker->convertJointCoordinatesToDepth(p.x, p.y, p.z, &m_pos[id].x, &m_pos[id].y);

                // pos torse
                p = user.getSkeleton().getJoint(nite::JOINT_TORSO).getPosition();
                m_utracker->convertJointCoordinatesToDepth(p.x, p.y, p.z, &m_posTorse[id].x, &m_posTorse[id].y);

                m_depths[id] = m_imgDepth[int((m_pos[id].y*m_width)+m_pos[id].x)];
            }
        }
    }
}

nite::Point3f Detection::to2D (const nite::Point3f& p) const
{
    nite::Point3f p2d;
    m_utracker->convertJointCoordinatesToDepth(p.x, p.y, p.z, &p2d.x, &p2d.y);
    return p2d;
}

Vec2 Detection::getPlayer (int n) const
{
    return m_pos[n];
}

Vec2 Detection::getPlayerTorse(int n) const
{
    return m_posTorse[n];
}

void Detection::setPlayerJoint (int id, nite::JointType jt)
{
    m_jtype[id] = jt;
}

nite::JointType Detection::getPlayerJoint (int id) const
{
    return m_jtype[id];
}

Detection::~Detection()
{
    if (m_imgDepth)
        delete[] m_imgDepth;
    if (m_imgRgb)
        delete[] m_imgRgb;

    delete m_utracker;
}
