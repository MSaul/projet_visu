#ifndef BONUS_HPP
#define BONUS_HPP

#include "pose.hpp"
#include "util.hpp"
#include <SFML/Graphics.hpp>
#include "Detection.hpp"

class Bonus : public sf::NonCopyable, public Observable
{
public:
    static Bonus &get();
    void update(double tmp);

    void reset();

    static const uint PHASE = 0;
    static const uint TIMER_PRE = 1;
    static const uint SQUEL = 2;
    static const uint TIMER_SQL = 3;

private:
    static Bonus *instance;

    Bonus();
    int squelOk();  // -1 pas ok, autre numero de l'equipe
    void applyEffect(int team);

private:
    double m_timerPre;
    double m_timerSql;
    uint m_phase;

    std::vector<Pose> m_squels;
    uint m_idPos;
};

class BonusView : public Observer
{
public:
    BonusView(const Bonus &b, const sf::Vector2f &posTimer, const sf::Vector2f &posSprite);

    bool update(uint code, const void *param);
    void draw(sf::RenderTarget &rt, const Detection &d);

protected:
    void drawPose(sf::RenderTarget& rt, const Vec2& base, const Pose& pose, sf::Color col);

    void changePhase(uint num);
    void changeTimerPre(double t);
    void changeTimerSql(double t);
    void changeSql(Pose *p);

private:
    sf::Text m_timer;
    uint m_phase;

    sf::Vector2f m_posTimer;

    Pose *m_pose;
};

#endif //BONUS_HPP
