#include "Detection.hpp"
#include "pose.hpp"
#include "util.hpp"
#include <cmath>

const Pose::BoneListElement Pose::boneList[BONES_NB] = {
#define X(a,b) {nite::a, nite::b, #a " - " #b}
    X(JOINT_HEAD, JOINT_NECK),

    X(JOINT_LEFT_SHOULDER, JOINT_LEFT_ELBOW),
    X(JOINT_LEFT_ELBOW, JOINT_LEFT_HAND),

    X(JOINT_RIGHT_SHOULDER, JOINT_RIGHT_ELBOW),
    X(JOINT_RIGHT_ELBOW, JOINT_RIGHT_HAND),

    X(JOINT_LEFT_SHOULDER, JOINT_RIGHT_SHOULDER),

    X(JOINT_LEFT_SHOULDER, JOINT_TORSO),
    X(JOINT_RIGHT_SHOULDER, JOINT_TORSO),

    X(JOINT_TORSO, JOINT_LEFT_HIP),
    X(JOINT_TORSO, JOINT_RIGHT_HIP),

    X(JOINT_LEFT_HIP, JOINT_RIGHT_HIP),

    X(JOINT_LEFT_HIP, JOINT_LEFT_KNEE),
    X(JOINT_LEFT_KNEE, JOINT_LEFT_FOOT),

    X(JOINT_RIGHT_HIP, JOINT_RIGHT_KNEE),
    X(JOINT_RIGHT_KNEE, JOINT_RIGHT_FOOT),
#undef X
};


Pose::Pose (const char* fn)
{
    load(fn);
}

void Pose::setSkeleton (const nite::Skeleton& s, const Detection& det)
{
    nite::Point3f torso = s.getJoint(nite::JOINT_TORSO).getPosition();
    nite::Point3f torso2d = det.to2D(torso);

    for (uint i = 0; i < JOINT_NB; i++) {
        nite::Point3f p = s.getJoint(nite::JointType(i)).getPosition();
        nite::Point3f p2d = det.to2D(p);
        m_joint[i].x = p2d.x - torso2d.x;
        m_joint[i].y = p2d.y - torso2d.y;
    }

    for (uint i = 0; i < LEN(Pose::boneList); i++) {
        nite::JointType a = Pose::boneList[i].a, b = Pose::boneList[i].b;
        nite::Point3f
            pa = det.to2D(s.getJoint(a).getPosition()),
            pb = det.to2D(s.getJoint(b).getPosition());

        nite::Point3f& p = m_bone[i];
        p.x = pb.x-pa.x;
        p.y = pb.y-pa.y;
        p.z = pb.z-pa.z;
        double len = sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
        p.x /= len;
        p.y /= len;
        p.z /= len;
    }
}

bool Pose::ressemble (const Pose& pose, float eps) const
{
    for (int i = 0; i < BONES_NB; i++) {
        if (0
            || fabs(m_bone[i].x - pose.m_bone[i].x) >= eps
            || fabs(m_bone[i].y - pose.m_bone[i].y) >= eps
            || fabs(m_bone[i].z - pose.m_bone[i].z) >= eps)
            return false;
    }
    return true;
}

void Pose::load (const char* fn)
{
    int r = 0;
    FILE* f = fopen(fn, "r");
    if (!f)
        E("can't open %s", fn);

    r += fread(m_bone, sizeof(m_bone), 1, f);
    r += fread(m_joint, sizeof(m_joint), 1, f);
    if (r != 2)
        E("fail");

    fclose(f);
}

void Pose::save (const char* fn) const
{
    int r = 0;
    FILE* f = fopen(fn, "w+");
    if (!f)
        E("can't open %s", fn);

    r += fwrite(m_bone, sizeof(m_bone), 1, f);
    r += fwrite(m_joint, sizeof(m_joint), 1, f);
    if (r != 2)
        E("fail");

    fclose(f);
}
