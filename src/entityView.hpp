#ifndef ENTITY_VIEW_HPP
#define ENTITY_VIEW_HPP

#include "util.hpp"
#include "entity.hpp"
#include <SFML/Graphics.hpp>

class EntityView : public Observer {
public:
    virtual ~EntityView(){}
    virtual bool update(uint code, const void *param);
    virtual void draw(sf::RenderTarget &rt) = 0;

protected:
    virtual void setAngle(double a){}
    virtual void rotate(double a){}
    virtual void setCenter(const sf::Vector2f &v){}
    virtual void move(const sf::Vector2f &v){}
    virtual void takeCollision(const Entity &e){}
    virtual void scale(const sf::Vector2f &s){}
};

class ObstacleView : public EntityView
{
public:
    ObstacleView(const Obstacle &o);
    void draw(sf::RenderTarget &rt);

protected:
    virtual void takeCollision(const Entity &e);

private:
    sf::RectangleShape m_rect;
    sf::Clock m_timeTouch;
    bool m_touch;
};

class WallView : public EntityView
{
public:
    WallView(const Entity &e, const sf::Color &colA, const sf::Color &colB);
    void draw(sf::RenderTarget &rt);
private:
    void takeCollision(const Entity &e);

    sf::RectangleShape m_rect;
    sf::Color m_colA;
    sf::Color m_colB;
    sf::Clock m_clock;
    bool m_touch;
};

class BallView : public EntityView
{
public:
    BallView(const Entity &e);
    void draw(sf::RenderTarget &rt);
    virtual bool update(uint code, const void *param);

protected:
    void setAngle(double a);
    void rotate(double a);
    void setCenter(const sf::Vector2f &v);
    void move(const sf::Vector2f &v);
    void takeCollision(const Entity &e);
    void reset();

private:
    sf::RectangleShape m_rect;
    sf::Clock m_clock;
};

class PadView : public EntityView
{
public:
    PadView(const Entity &e, uint id);
    void draw(sf::RenderTarget &rt);

private:
    void setCenter(const sf::Vector2f &v);
    void move(const sf::Vector2f &v);
    void takeCollision(const Entity &e);
    void scale(const sf::Vector2f &s);

    sf::RectangleShape m_rect;

    uint m_id;
    sf::Clock m_clock;
    bool m_touch;
};

class GoalView : public WallView
{
public:
    GoalView(const Goal &g);
};

#endif
