#include "Bonus.hpp"
#include "entity.hpp"
#include <cmath>

Bonus *Bonus::instance(0);

Bonus::Bonus() :
    m_timerPre(0), m_timerSql(0), m_phase(0)
{
    m_squels.push_back(Pose());
    m_squels.back().load("hands-up.pos");
    m_squels.push_back(Pose());
    m_squels.back().load("ecart_bras.pos");
    m_squels.push_back(Pose());
    m_squels.back().load("ecart_un_bras.pos");
    m_squels.push_back(Pose());
    m_squels.back().load("main_tete.pos");


    reset();
}

Bonus &Bonus::get()
{
    if(instance == 0)
        instance = new Bonus();
    return *instance;
}

void Bonus::update(double tmp)
{
    if(m_phase == 0) {
        // check squelette
        m_timerPre -= tmp;

        if(m_timerPre <= 0) {
            m_phase = 1;
            notify(PHASE, &m_phase);
        } else {
            int team = squelOk();
            if(team != -1) {

                applyEffect(team);
                // change de phase
                m_phase = 1;
                notify(PHASE, &m_phase);

            } else {
                // modif timer
                notify(TIMER_PRE, &m_timerPre);
            }
        }

    } else if(m_phase == 1) {
        // attente
        m_timerSql -= tmp;
        notify(TIMER_SQL, &m_timerSql);

        if(m_timerSql <= 0) {
            reset();
        }
    }
}

int Bonus::squelOk()
{
    int team = -1;
    for(int i = 1; i <= Detection::get().getPlayerNb(); i++) {
        const Pose &p = Detection::get().getPose(i);
        if(p.ressemble(m_squels[m_idPos], 0.5)) {
            // si plusieur joueurs font la pose on valide rien
            if(team != -1)
                return -1;

            team = i;
        }
    }

    return team;
}

void Bonus::applyEffect(int team)
{
    int r = m_rand(0, 3);

    std::cout << "BONUS : " << r << " ";
    if(r == 0) {
        // modif attribut pour diriger le pad
        nite::JointType jt = (nite::JointType) m_rand(nite::JOINT_RIGHT_FOOT);
        std::cout << "type : " << jt << std::endl;
        Detection::get().setPlayerJoint(team == 1 ? 2 : 1, jt);
    } else if(r == 1) {
        // augmente taille nous
        Pad *p = Pads::get().get(team);
        if(p != 0)
            p->scale(sf::Vector2f(1.2, 1.2));
    } else if(r == 2) {
        // diminue taille adv
        Pad *p = Pads::get().get(team == 1 ? 2 : 1);
        if(p != 0)
            p->scale(sf::Vector2f(0.8, 0.8));
    }
}

void Bonus::reset()
{
    m_timerPre = m_rand(10, 30);
    m_timerSql = m_rand(5, 15);
    m_phase = 0;
    m_idPos = m_rand(m_squels.size());
    notify(SQUEL, &m_squels[m_idPos]);
    notify(PHASE, &m_phase);
}

BonusView::BonusView(const Bonus &b, const sf::Vector2f &posTimer, const sf::Vector2f &posSprite) :
    m_timer("", getFont(), 20),
    m_phase(0),
    m_posTimer(posTimer),
    m_pose(0)
{
    changeTimerPre(10);
    m_timer.setPosition(posTimer);
    txtCenterized(m_timer);

    changePhase(0);
}

bool BonusView::update(uint code, const void *param)
{
    if(code == Bonus::PHASE) {
        changePhase(*(uint*) param);
    } else if(code == Bonus::TIMER_PRE) {
        changeTimerPre(*(double*)param);
    } else if(code == Bonus::SQUEL) {
        changeSql((Pose*)param);
    } else if(code == Bonus::TIMER_SQL) {
        changeTimerSql(*(double*)param);
    } else
        return false;
    return true;
}

void BonusView::draw(sf::RenderTarget &rt, const Detection &d)
{
    rt.draw(m_timer);
    if(m_phase == 0 && m_pose != 0) {
        for(int i = 1; i <= d.getPlayerNb(); i++) {
            drawPose(rt, d.getPlayerTorse(i), d.getPose(i), sf::Color(100, 100, 100));
            drawPose(rt, d.getPlayerTorse(i), *m_pose, sf::Color(100, 0, 0));
        }
    }
}

static void line (sf::RenderTarget& win, float x1, float y1, float x2, float y2, sf::Color col = sf::Color::Blue)
{
    sf::Vector2f ab(x2-x1, y2-y1);
    sf::RectangleShape rs(sf::Vector2f(sqrt(ab.x*ab.x + ab.y*ab.y), 5));
    rs.rotate(atan2(ab.y, ab.x) * 180 / M_PI);
    rs.setFillColor(col);
    rs.setPosition(sf::Vector2f(x1, y1));
    win.draw(rs);
}

extern sf::Vector2f scaleVect;
void BonusView::drawPose(sf::RenderTarget& rt, const Vec2& base, const Pose& p, sf::Color col)
{
    nite::Point3f torso = p.m_joint[nite::JOINT_TORSO];
    for (uint i = 0; i < BONES_NB; i++) {
        nite::JointType ja = Pose::boneList[i].a, jb = Pose::boneList[i].b;
        nite::Point3f pa = p.m_joint[ja];
        nite::Point3f pb = p.m_joint[jb];
        int bx = (torso.x + base.x) * scaleVect.x, by = (torso.y + base.y)  * scaleVect.y;
        line(rt, bx+pa.x*scaleVect.x, by+pa.y*scaleVect.y, bx+pb.x*scaleVect.x, by+pb.y*scaleVect.y, col);
        //printf("%f %f\n", pose.m_bone[i].x, by+pose.m_bone[i].y);
    }
}

void BonusView::changePhase(uint num)
{
    m_phase = num;
    if(m_phase == 0)
        m_timer.setColor(sf::Color(50, 150, 50));
    else if(m_phase == 1)
        m_timer.setColor(sf::Color(150, 50, 50));
}

void BonusView::changeTimerPre(double t)
{
    m_timer.setString("BONUS IN " + nts((int) t) + " SEC");
}

void BonusView::changeTimerSql(double t)
{
    m_timer.setString("STILL " + nts((int) t) + " SEC");
}

void BonusView::changeSql(Pose *p)
{
    m_pose = p;
}
