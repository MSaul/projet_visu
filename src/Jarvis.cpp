#include "Jarvis.hpp"

static int puissance(const sf::Vector2f &a, const sf::Vector2f &b, const sf::Vector2f &c)
{
    double r = (b.x - a.x)*(c.y - a.y) - (c.x - a.x)*(b.y - a.y);

    if (r < 0) {
        return 1;
    } else if (r > 0) {
        return 2;
    }
    return 0;
}

void jarvis(std::vector<sf::Vector2f> &ps, std::vector<sf::Vector2f> &cull)
{
    bool g_use_opti = false;

    if(ps.size() == 0)
        return;

    // on cherche le point G
    sf::Vector2f *G = &ps[0];
    uint i;
    for(i = 1; i < ps.size(); i++) {
        if(ps[i].x < G->x) {
            G = &ps[i];
        }
    }

    // on ajoute G a l'envelope
    sf::Vector2f *a_ajouter = G;
    sf::Vector2f *bout;
    uint k = 0;
    int ind_inut = 0;
    do {
        cull.push_back(*a_ajouter);

        bout = &ps[ind_inut];
        sf::Vector2f *courant = a_ajouter;
        uint j;
        for(j = ind_inut + 1; j < ps.size(); j++) {
            sf::Vector2f *vj = &ps[j];
            if(bout == a_ajouter ||
               puissance(*courant, *bout, *vj) == 1) {
                bout = vj;
            }
        }

        // on place tous les element qui peuvent etre ignorÃ© au debut du tableau
        if(g_use_opti) {
            for(k = ind_inut; k < ps.size(); k++) {
                if(puissance(*G, *bout, ps[k]) == 1) {
                    sf::Vector2f tmp = ps[ind_inut];
                    ps[ind_inut] = ps[k];
                    ps[k] = tmp;
                    ind_inut++;
                }
            }
        }

        a_ajouter = bout;
    } while(*bout != *G);
}
