#include "entityView.hpp"
#include <cmath>
#include "Score.hpp"

bool EntityView::update(uint code, const void *param)
{
    if(code == Entity::ROTATE) {
        rotate(*(double*) param);
    } else if(code == Entity::SET_ANGLE) {
        setAngle(*(double*) param);
    } else if(code == Entity::SET_CENTER) {
        setCenter(*(sf::Vector2f*) param);
    } else if(code == Entity::MOVE) {
        move(*(sf::Vector2f*) param);
    } else if(code == Entity::TAKE_COLLISION) {
        takeCollision(*(Entity*) param);
    } else if(code == Entity::SCALE) {
        scale(*(sf::Vector2f*)param);
    } else {
        return false;
    }

    return true;
}

ObstacleView::ObstacleView(const Obstacle &o) :
    m_rect(sf::Vector2f(distance(o.getLine().a, o.getLine().b), 10)), m_touch(false)
{
    m_rect.setFillColor(sf::Color(255, 0, 0, 100));
    m_rect.setOutlineThickness(1);
    m_rect.setOutlineColor(sf::Color::Black);
    m_rect.setPosition(o.getLine().a);

    const sf::Vector2f ab = o.getLine().b - o.getLine().a;
    m_rect.rotate(atan2(ab.y, ab.x) * 180 / M_PI);
}

void ObstacleView::draw(sf::RenderTarget &rt)
{
    if(m_touch && m_timeTouch.getElapsedTime().asSeconds() >= 0.1) {
        m_touch = false;
        m_rect.setFillColor(sf::Color(255, 0, 0, 100));
    }

    rt.draw(m_rect);
}

void ObstacleView::takeCollision(const Entity &e)
{
    m_touch = true;
    m_timeTouch.restart();
    m_rect.setFillColor(sf::Color(255, 127, 0, 100));
}

WallView::WallView(const Entity &e, const sf::Color &colA, const sf::Color &colB) :
    m_rect(e.getAABB().m_dim),
    m_colA(colA), m_colB(colB),
    m_touch(false)
{
    m_rect.setPosition(e.getAABB().m_pos);
    m_rect.setFillColor(m_colA);
}

void WallView::draw(sf::RenderTarget &rt)
{
    if(m_touch &&
       m_clock.getElapsedTime().asSeconds() >= 0.3) {
        m_clock.restart();
        m_rect.setFillColor(m_colA);
        m_touch = false;
    }

    rt.draw(m_rect);
}

void WallView::takeCollision(const Entity &e)
{
    m_rect.setFillColor(m_colB);
    m_touch = true;
    m_clock.restart();
}

BallView::BallView(const Entity &b) :
    m_rect(b.getAABB().m_dim)
{
    m_rect.setPosition(b.getAABB().m_pos);
    centerized(m_rect);

    m_rect.setRotation(b.getAngle() * 180 / M_PI);

    m_rect.setFillColor(sf::Color(255, 127, 0));
    m_rect.setOutlineThickness(1);
    m_rect.setOutlineColor(sf::Color::Black);
}

void BallView::draw(sf::RenderTarget &rt)
{
    rt.draw(m_rect);
}

bool BallView::update(uint code, const void *param)
{
    if(EntityView::update(code, param))
        return true;

    if(code == Ball::RESET)
        reset();
    else
        return false;

    return true;
}

void BallView::setAngle(double a)
{
    m_rect.setRotation(a * 180 / M_PI);
}

void BallView::rotate(double a)
{
    m_rect.rotate(a * 180 / M_PI);
}

void BallView::setCenter(const sf::Vector2f &v)
{
    m_rect.setPosition(v);
}

void BallView::move(const sf::Vector2f &v)
{
    m_rect.move(v);
}

void BallView::takeCollision(const Entity &e)
{
    if(m_clock.getElapsedTime().asSeconds() >= 0.2) {
        Particules::get().add(7, m_rect.getPosition(), sf::Vector2f(6, 2),
                              m_rect.getRotation() * M_PI / 180, M_PI / 3, sf::Color(255, 127, 0), 300, 150);
        Camera::get().shake(10);
    }

    m_clock.restart();
}

void BallView::reset()
{
    Particules::get().add(20, m_rect.getPosition(), sf::Vector2f(6, 2),
                          0, 360, sf::Color(255, 127, 0), 300, 150);
}

PadView::PadView(const Entity &e, uint id) :
    m_rect(e.getAABB().m_dim), m_id(id), m_touch(false)
{
    m_rect.setPosition(e.getAABB().m_pos);
    centerized(m_rect);

    m_rect.setRotation(e.getAngle() * 180 / M_PI);
    m_rect.setFillColor(Score::getColor(id));
}

void PadView::draw(sf::RenderTarget &rt)
{
    if(m_touch && m_clock.getElapsedTime().asSeconds() > 0.3) {
        m_touch = false;
        m_rect.setFillColor(Score::getColor(m_id));
    }

    rt.draw(m_rect);
}

void PadView::setCenter(const sf::Vector2f &v)
{
    m_rect.setPosition(v);
}

void PadView::move(const sf::Vector2f &v)
{
    m_rect.move(v);
}

void PadView::takeCollision(const Entity &e)
{
    m_touch = true;
    m_clock.restart();
    m_rect.setFillColor(sf::Color(255, 127, 0));
}

void PadView::scale(const sf::Vector2f &s)
{
    m_rect.scale(s);
}


GoalView::GoalView(const Goal &g) :
    WallView(g, Score::getColor(1 - g.getId()), Score::getColor(g.getId()))
{

}
