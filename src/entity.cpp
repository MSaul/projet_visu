#include "entity.hpp"
#include <cmath>
#include "constant.hpp"
#include "Score.hpp"

Line::Line(const sf::Vector2f &a, const sf::Vector2f &b) :
    a(a), b(b)
{}

static bool lineIntersect(const sf::Vector2f &a1, const sf::Vector2f &a2, const sf::Vector2f &b1, const sf::Vector2f &b2, sf::Vector2f &intersection)
{
    intersection = sf::Vector2f(0, 0);

    sf::Vector2f b = a2 - a1;
    sf::Vector2f d = b2 - b1;
    float bDotDPerp = b.x * d.y - b.y * d.x;

    // if b dot d == 0, it means the lines are parallel so have infinite intersection points
    if (bDotDPerp == 0)
        return false;

    sf::Vector2f c = b1 - a1;
    float t = (c.x * d.y - c.y * d.x) / bDotDPerp;
    if (t < 0 || t > 1)
        return false;

    float u = (c.x * b.y - c.y * b.x) / bDotDPerp;
    if (u < 0 || u > 1)
        return false;

    intersection = a1 + t * b;

    return true;
}

bool Line::collision(const Line &l, sf::Vector2f &inter)
{
    return lineIntersect(a, b, l.a, l.b, inter);
}

AABB::AABB(const sf::Vector2f &pos, const sf::Vector2f &dim) :
    m_pos(pos), m_dim(dim)
{
    if(dim.x < 0) {
        m_pos.x = m_pos.x + dim.x;
        m_dim.x = -dim.x;
    }
    if(dim.y < 0) {
        m_pos.y = m_pos.y + dim.y;
        m_dim.y = -dim.y;
    }
}

void AABB::move(const sf::Vector2f &v)
{
    m_pos += v;
}

void AABB::setPos(const sf::Vector2f &p)
{
    m_pos = p;
}

bool AABB::collision(const AABB &b) const
{
    const sf::Vector2f tposb = m_pos + m_dim;
    const sf::Vector2f bposb = b.m_pos + b.m_dim;

    return !(bposb.x < m_pos.x || b.m_pos.x > tposb.x ||
             bposb.y < m_pos.y || b.m_pos.y > tposb.y);
}
//

std::vector<Entity*> Entity::m_entities;
uint Entity::idMax = 0;

Entity::Entity(const sf::Vector2f &pos, const sf::Vector2f &dim) :
    m_aabb(pos, dim), m_angle(0), m_testDeplacLat(true), m_id(idMax++)
{
    m_entities.push_back(this);
}

Entity::~Entity()
{
    for(std::vector<Entity*>::iterator it = m_entities.begin();
        it != m_entities.end(); it++) {
        if((*it)->getId() == m_id) {
            m_entities.erase(it);
            break;
        }
    }
}

void Entity::update(double t)
{}

void Entity::takeCollision(Entity &e, bool fromYou)
{
    notify(TAKE_COLLISION, &e);
}

bool Entity::collision(Entity &e)
{
    return m_aabb.collision(e.getAABB());
}

bool Entity::baseMove(const sf::Vector2f &v)
{
    m_aabb.move(v);

    bool coll = false;
    for(uint i = 0; i < m_entities.size(); i++) {
        if(i != m_id &&  collision(*m_entities[i]) && m_entities[i]->collision(*this)) {
            m_entities[i]->takeCollision(*this, false);
            takeCollision(*m_entities[i], true);

            coll = true;
        }
    }

    if(coll) {
        m_aabb.move(-v);
        if(m_testDeplacLat &&
           v.x != 0 && v.y != 0) {
            baseMove(sf::Vector2f(v.x, 0));
            baseMove(sf::Vector2f(0, v.y));
        }

        return false;
    } else {
        notify(MOVE, &v);
    }

    return true;
}

void Entity::forceSetCenter(const sf::Vector2f &c)
{
    m_aabb.m_pos = c - multVector(m_aabb.m_dim, 0.5);
    notify(SET_CENTER, &c);
}

void Entity::scale(const sf::Vector2f &s)
{
    m_aabb.m_dim.x *= s.x;
    m_aabb.m_dim.y *= s.y;
    notify(SCALE, &s);
}

void Entity::move(const sf::Vector2f &v, double distMax)
{
    double norm = sqrt(v.x*v.x + v.y*v.y);
    if(norm == 0)        return;

    uint nbDep = 1;
    sf::Vector2f vMax(v);
    if(distMax != 0) {
        vMax = sf::Vector2f(v.x*distMax/norm, v.y*distMax/norm);
        nbDep = norm / distMax;
    }

    // plusieur petit deplacement
    for(uint i = 0; i < nbDep; i++) {
        if(!baseMove(vMax))
            return;
    }

    // dernier petit deplacement car pas norm pas forcement multiple de distMax
    double diff = norm - (nbDep * distMax);
    if(diff != 0) {
        baseMove(sf::Vector2f(v.x*diff/norm, v.y*diff/norm));
    }
}

void Entity::setPos(const sf::Vector2f &p)
{
    sf::Vector2f tmpPos = m_aabb.m_pos;

    m_aabb.setPos(p);
    bool coll = false;
    for(uint i = 0; i < m_entities.size(); i++) {
        if(i != m_id && collision(*m_entities[i])) {
            m_entities[i]->takeCollision(*this, false);
            takeCollision(*m_entities[i], true);

            coll = true;
        }
    }

    if(coll)
        m_aabb.setPos(tmpPos);
    else {
        sf::Vector2f c = p + multVector(m_aabb.m_dim, .5);
        notify(SET_CENTER, &c);
    }
}

void Entity::setPosMove(const sf::Vector2f &p, double smoothRatio)
{
    const sf::Vector2f diff = multVector(p - m_aabb.m_pos, smoothRatio);
    move(diff, 1.);
}

void Entity::setCenter(const sf::Vector2f &c)
{
    setPos(c - (multVector(m_aabb.m_dim, .5)));
}

void Entity::setCenterMove(const sf::Vector2f &c, double smoothRatio)
{
    setPosMove(c - (multVector(m_aabb.m_dim, .5)), smoothRatio);
}

void Entity::setAngle(double a)
{
    m_angle = a;
    notify(SET_ANGLE, &a);
}

void Entity::rotate(double a)
{
    m_angle += a;
    notify(ROTATE, &a);
}

double Entity::getAngle() const
{
    return m_angle;
}

const AABB &Entity::getAABB() const
{
    return m_aabb;
}

sf::Vector2f Entity::getCenter() const
{
    return m_aabb.m_pos + multVector(m_aabb.m_dim, 0.5);
}

uint Entity::getId()
{
    return m_id;
}

uint Obstacle::currentLvl = 0;

Obstacle::Obstacle(const sf::Vector2f &a, const sf::Vector2f &b, uint lvl) :
    Entity(a, b-a), m_line(a, b), m_lvl(lvl)
{
}

bool Obstacle::collision(Entity &e, sf::Vector2f &p)
{
    if(m_lvl != currentLvl)
        return false;

    const AABB &box = e.getAABB();

    Line a(box.m_pos + sf::Vector2f(0, 0), box.m_pos + sf::Vector2f(box.m_dim.x, 0));
    Line b(box.m_pos + sf::Vector2f(box.m_dim.x, 0), box.m_pos + sf::Vector2f(0, box.m_dim.y));
    Line c(box.m_pos + sf::Vector2f(0, box.m_dim.y), box.m_pos + sf::Vector2f(box.m_dim.x, box.m_dim.y));
    Line d(box.m_pos + sf::Vector2f(0, 0), box.m_pos + sf::Vector2f(0, box.m_dim.y));

    return m_line.collision(a, p)
        || m_line.collision(b, p)
        || m_line.collision(c, p)
        || m_line.collision(d, p);
}

bool Obstacle::collision(Entity &e)
{
    sf::Vector2f p;
    return collision(e, p);
}

Pad::Pad(const AABB &limit) :
    Entity(sf::Vector2f(0, 0), sf::Vector2f(30, 100)), m_limit(limit),
    m_lastReset(0)
{
    m_limit.m_pos += getAABB().m_dim;
    m_limit.m_dim -= getAABB().m_dim;
    m_limit.m_dim -= getAABB().m_dim;

    reset();
}

void Pad::update(double t)
{
    m_lastReset += t;
}

void Pad::move(const sf::Vector2f &v, double dMax)
{
    if(m_lastReset <= 0.2)
        return;

    sf::Vector2f backPos = getAABB().m_pos;

    Entity::move(v, dMax);
    if(!m_limit.collision(getAABB()))
        Entity::setPos(backPos);
}

void Pad::setPos(const sf::Vector2f &p)
{
    const sf::Vector2f tmpPos = getAABB().m_pos;

    Entity::setPos(p);
    if(!m_limit.collision(getAABB()))
        Entity::setPos(tmpPos);
}

void Pad::reset()
{
    const sf::Vector2f npos = m_limit.m_pos + multVector(m_limit.m_dim, 0.5);
    forceSetCenter(npos);
    m_lastReset = 0;
}

Pads *Pads::instance(0);

Pads &Pads::get()
{
    if(instance == 0) {
        instance = new Pads();
    }

    return *instance;
}

Pads::~Pads()
{
    for(uint i = 0; i < m_pads.size(); i++)
        delete m_pads[i];
}

Pad *Pads::create(const AABB &limit)
{
    m_pads.push_back(new Pad(limit));
    return m_pads[m_pads.size() - 1];
}

Pad *Pads::get(uint id)
{
    if(id >= size())
        return 0;
    return m_pads[id];
}

const Pad *Pads::get(uint id) const
{
    if(id >= size())
        return 0;
    return m_pads[id];
}

uint Pads::size() const
{
    return m_pads.size();
}

const std::vector<Pad*> &Pads::getPads() const
{
    return m_pads;
}

std::vector<Pad*> &Pads::getPads()
{
    return m_pads;
}

Ball::Ball(const sf::Vector2f &pos, double speed) :
    Entity(pos, sf::Vector2f(15, 15)),
    m_speed(0), M_MIN_SPEED(speed), M_MAX_SPEED(speed * 6),
    m_lastSpeedUp(0.), m_lastReset(0)
{
    testDeplacLat(false);
    reset();
}

void Ball::update(double t)
{
    // maj du temps
    m_lastSpeedUp += t;
    m_lastReset += t;

    // deplacement
    if(t >= 1 / 24.)       t = 0;   // interdiction des gros d�placement
    double d = m_speed * t;
    sf::Vector2f vv(cos(getAngle()) * d,
                    sin(getAngle()) * d);
    move(vv, 1);
}

void Ball::takeCollisionNorm(const Entity &e)
{
    // vecteur de la direction de la balle
    double dist = 2;
    sf::Vector2f vv(cos(getAngle()) * dist,
                    sin(getAngle()) * dist);
    double normVV = sqrt(vv.x*vv.x + vv.y*vv.y);
    vv.x /= normVV;
    vv.y /= normVV;

    // direction de la normal
    sf::Vector2f norm(0, 0);

    const sf::Vector2f tposa = getAABB().m_pos,
        eposa = e.getAABB().m_pos;
    const sf::Vector2f tposb = tposa + getAABB().m_dim,
        eposb = eposa + e.getAABB().m_dim;

    if(tposa.x < eposa.x && tposb.x > eposa.x)            norm.x = -1;
    if(tposa.x < eposb.x && tposb.x > eposb.x)            norm.x = 1;
    if(tposa.y < eposa.y && tposb.y > eposa.y)            norm.y = -1;
    if(tposa.y < eposb.y && tposb.y > eposb.y)            norm.y = 1;
    if(norm.x == 0 && norm.y == 0)   return;

    double normNorm = sqrt(norm.x*norm.x + norm.y*norm.y);
    norm.x /= normNorm;
    norm.y /= normNorm;

    double dot = -(norm.x * vv.x + norm.y * vv.y) * 2;
    norm.x *= dot;
    norm.y *= dot;
    norm += vv;

    setAngle(atan2(norm.y, norm.x));
}

void Ball::takeCollisionPad(const Entity &pad)
{
    sf::Vector2f padCenter = pad.getCenter();
    sf::Vector2f ballCenter = getCenter();

    sf::Vector2f pb = ballCenter - padCenter;
    setAngle(atan2(pb.y, pb.x));
}

static int puissance(const sf::Vector2f &a, const sf::Vector2f &b, const sf::Vector2f &c)
{
    double r = (b.x - a.x)*(c.y - a.y) - (c.x - a.x)*(b.y - a.y);

    if (r < 0) {
        return 1;
    } else if (r > 0) {
        return 2;
    }
    return 0;
}

void Ball::takeCollisionObs(const Obstacle &o)
{
    double dist = 2;
    sf::Vector2f vv(cos(getAngle()) * dist,
                    sin(getAngle()) * dist);
    double normVV = sqrt(vv.x*vv.x + vv.y*vv.y);
    vv.x /= normVV;
    vv.y /= normVV;

    sf::Vector2f ab = o.getLine().a - o.getLine().b;
    sf::Vector2f normal(-ab.y, ab.x);
    if(puissance(o.getLine().a, o.getLine().b, getCenter()) == 2) {
        normal = multVector(normal, -1);
    }

    double normNormal = sqrt(normal.x*normal.x + normal.y*normal.y);
    normal.x /= normNormal;
    normal.y /= normNormal;

    double dot = -(normal.x * vv.x + normal.y * vv.y) * 2;
    normal.x *= dot;
    normal.y *= dot;
    normal += vv;

    setAngle(atan2(normal.y, normal.x));
}

void Ball::takeCollision(Entity &e, bool fromYou)
{
    Pad *p = dynamic_cast<Pad*>(&e);
    Obstacle *o = dynamic_cast<Obstacle*>(&e);
    if(p != 0)       takeCollisionPad(e);
    else if(o != 0)  takeCollisionObs(*o);
    else             takeCollisionNorm(e);

    /* augmentation de la vitesse � chaque collision */
    if(m_speed == 0) {
        if(m_lastReset >= 1.)
            m_speed = M_MIN_SPEED;
    } else if(m_lastSpeedUp >= 1. && m_speed < M_MAX_SPEED) {
        m_speed += (M_MAX_SPEED - M_MIN_SPEED) / 50.;
        m_lastSpeedUp = 0;
    }

    Entity::takeCollision(e, fromYou);
}

void Ball::reset()
{
    setCenter(sf::Vector2f(WINDOW_WIDTH / 2., WINDOW_HEIGHT / 2.));
    m_speed = 0;
    setAngle(0);
    m_lastReset = 0.;
    notify(RESET);
}

Goal::Goal(const sf::Vector2f &pos, uint id) :
    Entity(pos, sf::Vector2f(10, 200)), m_id(id)
{
}

uint Goal::getId() const
{
    return m_id;
}

void Goal::takeCollision(Entity &e, bool fromYou)
{
    Ball *b = dynamic_cast<Ball*>(&e);
    if(b != 0) {
        Score::get().add(m_id, 1);

        for(uint i = 0; i < Pads::get().size(); i++) {
            Pads::get().get(i)->reset();
        }
        b->reset();

        notify(TAKE_COLLISION, &e);
    }
}
