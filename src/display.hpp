#ifndef DISPLAY
#define DISPLAY

#include <vector>
#include "Detection.hpp"
#include "entityView.hpp"

class Display {
public:
    Display();
    void draw();

protected:
    void calcBack();

private:
    static const uint MAX_LVL = 30;
    uint m_maxDepthLevel;
    std::vector<Obstacle*> m_os;
    std::vector<ObstacleView*> m_ovs[MAX_LVL];
};


#endif
