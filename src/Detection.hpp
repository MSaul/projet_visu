#ifndef _DETECTION_H_
#define _DETECTION_H_

#include "NiTE.h"
#include "pose.hpp"

struct Vec2
{
    float x, y;
};

#define DEFAULT_JOINT nite::JOINT_RIGHT_HAND

// nite::JointType

//     JOINT_HEAD,
//     JOINT_NECK,
//     JOINT_LEFT_SHOULDER,
//     JOINT_RIGHT_SHOULDER,
//     JOINT_LEFT_ELBOW,
//     JOINT_RIGHT_ELBOW,
//     JOINT_LEFT_HAND,
//     JOINT_RIGHT_HAND,
//     JOINT_TORSO,
//     JOINT_LEFT_HIP,
//     JOINT_RIGHT_HIP,
//     JOINT_LEFT_KNEE,
//     JOINT_RIGHT_KNEE,
//     JOINT_LEFT_FOOT,
//     JOINT_RIGHT_FOOT,

typedef unsigned short uint16_t;
typedef unsigned char uint8_t;

typedef uint16_t depth_pixel_t;
typedef uint8_t rgb_pixel_t[3];

class Detection
{
public:
    static Detection &get();

    int openDevice();

    int getPlayerNb() const;
    void computeNextPos();
    Vec2 getPlayer (int n) const;
    Vec2 getPlayerTorse(int n) const;
    int getDepthPlayer (int);
    int getWidth()  const              { return m_width; }
    int getHeight() const              { return m_height; }
    rgb_pixel_t* getRGB () const       { return m_imgRgb; }
    depth_pixel_t* getDepth () const   { return m_imgDepth; }
    int getDepthMax () const           { return m_depthMax; }
    const Pose& getPose (int id) const { return m_pose[id]; }
    void setPlayerJoint (int id, nite::JointType jt);
    nite::JointType getPlayerJoint (int id) const;
    nite::Point3f to2D (const nite::Point3f&) const;

    virtual ~Detection();

private:
    Detection();
    Detection(const Detection &);
    Detection operator=(const Detection &);

    static Detection *instance;

private:
    int m_playerNb;
    int m_width, m_height, m_depthMax;
    depth_pixel_t* m_imgDepth;
    rgb_pixel_t* m_imgRgb;

    Vec2 m_pos[10]; // position (x,y) écran d'un joueur
    Vec2 m_posTorse[10]; // position (x,y) écran du torse d'un joueur
    int m_depths[10];
    Pose m_pose[10]; // skelette (Pose) d'un joueur
    nite::JointType m_jtype[10];

    openni::Device m_device;
    openni::VideoStream m_streamRgb;
    nite::UserTracker* m_utracker;
};

#endif /* _DETECTION_H_ */
