#include "util.hpp"
#include <cmath>
#include "constant.hpp"

bool Observer::update(uint code, const void* param)
{
    return false;
}

sf::Vector2f centerized(sf::RectangleShape &rect)
{
    sf::Vector2f dimd2(rect.getSize().x / 2, rect.getSize().y / 2);
    rect.setOrigin(dimd2);
    rect.move(dimd2);

    return dimd2;
}

void txtCenterized(sf::Text &t)
{
    const sf::Vector2f dimD2((int) (t.getLocalBounds().width / 2.),
                             (int) (t.getLocalBounds().height / 2.));
    t.setOrigin(dimD2);
}

sf::Vector2f multVector(const sf::Vector2f &v, double m)
{
    return sf::Vector2f(v.x * m, v.y * m);
}

double distance(const sf::Vector2f &a, const sf::Vector2f &b)
{
    const sf::Vector2f v(b-a);
    return sqrt(v.x*v.x + v.y*v.y);
}

double m_rand()
{
    static bool init = false;
    if(!init) {
        int seed = time(0);
        srand(seed);
        std::cout << "seed " << seed << std::endl;
        init = true;
    }

    return ((double)(rand())) / RAND_MAX;
}

double m_rand(double max)
{
    return m_rand() * max;
}

double m_rand(double min, double max)
{
    return m_rand(max - min) + min;
}

sf::Vector2f m_rand(const sf::Vector2f &v, double min, double max)
{
    return sf::Vector2f(v.x + m_rand(min, max), v.y + m_rand(min, max));
}

sf::Font &getFont()
{
    static sf::Font *font = 0;
    if(font == 0) {
        font = new sf::Font();
        font->loadFromFile("visitor1.ttf");
    }
    return *font;
}

Particule::Particule(const sf::Vector2f &pos, const sf::Vector2f &dim, float angle,
                     const sf::Color &c, float speed, float maxDistance) :
    m_rs(dim), M_DIST_MAX(maxDistance),
    m_distDone(0), m_dead(false),
    m_angle(angle), m_speed(speed)
{
    m_rs.setFillColor(c);
    m_rs.setPosition(pos);
    m_rs.setRotation(angle*180/M_PI);
}

Particule::~Particule()
{
}

void Particule::draw(sf::RenderTarget &rt)
{
    rt.draw(m_rs);
}

void Particule::update(float t)
{
    float distance = t * m_speed;
    sf::Vector2f vdep(distance * cos(m_angle), distance * sin(m_angle));
    m_rs.move(vdep);

    m_distDone += sqrt((vdep.x * vdep.x) + (vdep.y * vdep.y));
    if(m_distDone >= M_DIST_MAX)
        m_dead = true;
    else {
        const double ratio = m_distDone / M_DIST_MAX;
        const int MIN = 75;

        m_rs.setFillColor(sf::Color(m_rs.getFillColor().r,
                                    m_rs.getFillColor().g,
                                    m_rs.getFillColor().b,
                                    MIN + (1 - ratio) * (255 - MIN)));
    }
}

bool Particule::isDead() const
{
    return m_dead;
}

Particules *Particules::instance(0);

Particules &Particules::get()
{
    if(instance == 0)
        instance = new Particules();
    return *instance;
}

Particules::Particules()
{}

Particules::~Particules()
{
    for(std::list<Particule*>::iterator it = m_particules.begin(); it != m_particules.end(); ++it)
        delete (*it);
}

void Particules::add(uint nb, const sf::Vector2f &pos, const sf::Vector2f &dim, float angle,
                     float openAngle, const sf::Color &c, float speed, float rad)
{
    const float angleMin = angle - openAngle / 2.;
    const float angleMax = angle + openAngle / 2.;

    for(uint i = 0; i < nb; i++) {
        const float angleRand = m_rand(angleMin, angleMax);

        Particule *p = new Particule(pos, dim, angleRand, c, speed, rad);
        m_particules.push_back(p);
    }
}

void Particules::draw(sf::RenderTarget &rt)
{
    double t = m_clock.restart().asSeconds();
    if(t == 0.0)
        return;

    for(std::list<Particule*>::iterator it = m_particules.begin(); it != m_particules.end(); ) {
        (*it)->update(t);
        (*it)->draw(rt);
        if((*it)->isDead()) {
            delete *it;
            it = m_particules.erase(it);
        } else {
            ++it;
        }
    }
}

void Particules::clear()
{
    for(std::list<Particule*>::iterator it = m_particules.begin(); it != m_particules.end(); ++it)
        delete (*it);
    m_particules.clear();
}


Camera *Camera::instance = 0;

Camera::Camera() :
    m_view(sf::FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT)),
    m_tmp(0), m_shake(false), m_vectShake(0, 0)
{}

Camera &Camera::get()
{
    if(instance == 0)
        instance = new Camera;
    return *instance;
}

void Camera::update()
{
    if(m_shake) {
        m_tmp += m_clock.restart().asSeconds();

        if(m_tmp >= 0.1) {
            m_tmp = 0;
            if((uint) m_rand(0, 2) == 0)   m_vectShake.x *= -1;
            else                           m_vectShake.y *= -1;

            static const float RATIO_DIM = 0.3;
            m_vectShake.x *= RATIO_DIM;
            m_vectShake.y *= RATIO_DIM;

            if(fabs(m_vectShake.x) <= 1 && fabs(m_vectShake.y) <= 1) {
                reinitShake();
            }
        }
    }

    m_view.setCenter(sf::Vector2f(WINDOW_WIDTH / 2., WINDOW_HEIGHT / 2.) +
                     m_vectShake);
}

void Camera::shake(float puissance)
{
    float xt = m_rand(puissance);
    float yt = puissance - xt;

    if(m_vectShake.x < 0)     xt *= -1;
    if(m_vectShake.y < 0)     yt *= -1;

    m_vectShake.x += xt;
    m_vectShake.y += yt;

    m_shake = true;
}

void Camera::reinitShake()
{
    m_vectShake = sf::Vector2f(0, 0);
    m_shake = false;
}
