#ifndef UTIL_HPP
#define UTIL_HPP

#include <SFML/Graphics.hpp>
#include <iostream>
#include <list>
#include <sstream>

#include <cstdio>
#include <cstdlib>

#define W(...) fprintf(stderr, "%s:%d @ %s :: ", __FILE__, __LINE__, __FUNCTION__), fprintf(stderr, __VA_ARGS__), fputc('\n', stderr)
#define E(...) W(__VA_ARGS__), exit(1)
#define LEN(x) (sizeof(x)/sizeof(x[0]))

typedef unsigned int uint;
//
class Observer
{
public:
    virtual bool update(uint code, const void* param = 0);
};

class Observable
{
public:
    Observable() : m_obs(0) {}

    void setObserver(Observer *o) {
        m_obs = o;
    }

    void notify(uint code, const void* param = 0) {
        if(m_obs != 0)
            m_obs->update(code, param);
    }

private:
    Observer *m_obs;
};

//
sf::Vector2f centerized(sf::RectangleShape &rect);
void txtCenterized(sf::Text &t);
sf::Vector2f multVector(const sf::Vector2f &v, double m);
double distance(const sf::Vector2f &a, const sf::Vector2f &b);

template <typename T>
std::string nts(T n)
{
    std::stringstream ss;
    ss << n;
    return ss.str();
}

template<typename T>
void printVector(const sf::Vector2<T> v)
{
    std::cout << v.x << " " << v.y << std::endl;
}

//
double m_rand();        // 0 < res < 1
double m_rand(double max);
double m_rand(double min, double max);
sf::Font &getFont();

template<typename T>
T clamp(const T &val, const T &min, const T &max)
{
    if(val < min)
        return min;
    if(val > max)
        return max;
    return val;
}

//
class Particule
{
public:
    // angle en radian
    Particule(const sf::Vector2f &pos, const sf::Vector2f &dim, float angle, const sf::Color &c,
              float speed, float maxDistance);

    ~Particule();

    void draw(sf::RenderTarget &rt);
    void update(float t);

    bool isDead() const;

private:
    sf::RectangleShape m_rs;

    const float M_DIST_MAX;
    float m_distDone;
    bool m_dead;

    float m_angle;
    float m_speed;
};

class Particules : sf::NonCopyable
{
public:
    ~Particules();

    static Particules &get();

    void add(uint nb, const sf::Vector2f &pos, const sf::Vector2f &dim, float angle,
             float openAngle, const sf::Color &c, float speed, float rad);

    void draw(sf::RenderTarget &rt);

    void clear();

private:
    Particules();
    static Particules *instance;

    std::list<Particule*> m_particules;

    sf::Clock m_clock;
};

//
class Camera : public sf::NonCopyable
{
public:
    static Camera &get();

    void update();

    void shake(float puissance);
    void reinitShake();

    const sf::View getView() const{return m_view;}

private:
    Camera();

    static Camera *instance;

    sf::View m_view;

    sf::Clock m_clock;
    float m_tmp;
    bool m_shake;
    sf::Vector2f m_vectShake;
};


#endif
