#ifndef JARVIS_HPP
#define JARVIS_HPP

#include "util.hpp"

void jarvis(std::vector<sf::Vector2f> &ps, std::vector<sf::Vector2f> &cull);

#endif // JARVIS_HPP
