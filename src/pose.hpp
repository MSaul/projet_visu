#ifndef _POSE_H_
#define _POSE_H_

#include "util.hpp"
#include "NiTE.h"

#define BONES_NB 15
#define JOINT_NB 15

class Detection;
class Pose
{
public:
    const static struct BoneListElement {
        nite::JointType a, b;
        const char* str;
    } boneList[BONES_NB];

    Pose () { memset(m_bone, 0, sizeof(m_bone)); }
    Pose (const char* filename);

    void setSkeleton (const nite::Skeleton& s, const Detection&);
    bool ressemble (const Pose&, float eps = .001) const;

    void load (const char* filename);
    void save (const char* filename) const;

    //private:
    nite::Point3f m_bone[BONES_NB];
    nite::Point3f m_joint[JOINT_NB];
};

#endif /* _POSE_H_ */
