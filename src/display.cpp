#include "display.hpp"
#include "entityView.hpp"
#include "constant.hpp"
#include "Score.hpp"
#include "Bonus.hpp"
#include "Jarvis.hpp"
#include <cmath>
#include <stack>

sf::Vector2f scaleVect(1, 1);

Display::Display() : m_maxDepthLevel(0)
{
    Detection::get().openDevice();
    Detection::get().computeNextPos();

    depth_pixel_t *img = Detection::get().getDepth();
    for(int i = 0; i < Detection::get().getHeight() * Detection::get().getWidth(); i++) {
            if(img[i] > m_maxDepthLevel)
                m_maxDepthLevel = img[i];
    }

//    calcBack();
}

static bool border(const sf::Vector2f &pos, int width, int height, depth_pixel_t *img, int threshold)
{
    for(int x = -1; x <= 1; x++) {
        for(int y = -1; y <= 1; y++) {
            if(x == 0 && y == 0)
                continue;

            sf::Vector2f npos = pos + sf::Vector2f(x, y);
            if(npos.x < 0 || npos.x >= width ||
               npos.y < 0 || npos.y >= height)
                continue;

            if(img[(int) ((npos.y*width)+npos.x)] < threshold) {
                return true;
            }
        }
    }
    return false;
}

static void explode(const sf::Vector2f &pos, int *ident, int width, int height, depth_pixel_t *img, int c, int threshold, std::vector<sf::Vector2f> &ps)
{
    std::stack<sf::Vector2f> stack;
    stack.push(pos);

    sf::Vector2f p;
    while(!stack.empty()) {
        p = stack.top();
        stack.pop();

        int id = (p.y*width) + p.x;
        if(p.x < 0 || p.x >= width ||
           p.y < 0 || p.y >= height ||
           ident[id] != 0 || img[id] < threshold ||
           !border(p, width, height, img, threshold))
            continue;

        ident[id] = c;
        ps.push_back(p);

        for(int i = -1; i <= 1; i++) {
            for(int j = -1; j <= 1; j++) {
                if(i == 0 && j == 0)
                    continue;
                stack.push(p + sf::Vector2f(i, j));
            }
        }
    }
}

void Display::calcBack()
{
    double step = m_maxDepthLevel / MAX_LVL;
    double threshold = 0;
    for(uint iThrs = 0; iThrs < MAX_LVL; iThrs++) {
        threshold += step;

        depth_pixel_t *img = Detection::get().getDepth();
        int ident[Detection::get().getHeight() * Detection::get().getWidth()];
        int c = 0;
        sf::Vector2f scale(WINDOW_WIDTH  / (double) Detection::get().getWidth(),
                           WINDOW_HEIGHT / (double) Detection::get().getHeight());

        for(int i = 0; i < Detection::get().getHeight() * Detection::get().getWidth(); i++)
            ident[i] = 0;

        for(int y = 0; y < Detection::get().getHeight(); y++) {
            for(int x = 0; x < Detection::get().getWidth(); x++) {
                int id = y*Detection::get().getWidth() + x;

                if(ident[id] == 0 &&
                   img[id] > threshold) {
                    // segmentation
                    c++;
                    std::vector<sf::Vector2f> ps;
                    int area;
                    explode(sf::Vector2f(x, y), ident, Detection::get().getWidth(),
                            Detection::get().getHeight(), img, c, threshold, ps);

                    // enveloppe convexe
//                    std::vector<sf::Vector2f> cull;
//                    jarvis(ps, cull);

                    // ajout des obstacles
                    for(uint i = 0; i < ps.size(); i++) {
                        int ia = i;
                        int ib = (i+1)%ps.size();

                        sf::Vector2f a(ps[ia].x * scale.x, ps[ia].y * scale.y);
                        sf::Vector2f b(ps[ib].x * scale.x, ps[ib].y * scale.y);
                        m_os.push_back(new Obstacle(a, b, iThrs));
                        m_ovs[iThrs].push_back(new ObstacleView(*m_os[m_os.size() - 1]));
                    }
                }
            }
        }
    }
}

static void line (sf::RenderWindow& win, float x1, float y1, float x2, float y2, sf::Color col = sf::Color::Blue)
{
    sf::Vector2f ab(x2-x1, y2-y1);
    sf::RectangleShape rs(sf::Vector2f(sqrt(ab.x*ab.x + ab.y*ab.y), 5));
    rs.rotate(atan2(ab.y, ab.x) * 180 / M_PI);
    rs.setFillColor(col);
    rs.setPosition(sf::Vector2f(x1, y1));
    win.draw(rs);
}

static void drawPose (sf::RenderWindow& win, const Vec2& base, const Pose& pose, sf::Color col)
{
    nite::Point3f torso = pose.m_joint[nite::JOINT_TORSO];
    for (uint i = 0; i < BONES_NB; i++) {
        nite::JointType ja = Pose::boneList[i].a, jb = Pose::boneList[i].b;
        nite::Point3f pa = pose.m_joint[ja];
        nite::Point3f pb = pose.m_joint[jb];
        int bx = (torso.x + base.x) * scaleVect.x, by = (torso.y + base.y)  * scaleVect.y;
        line(win, bx+pa.x*scaleVect.x, by+pa.y*scaleVect.y, bx+pb.x*scaleVect.x, by+pb.y*scaleVect.y, col);
        //printf("%f %f\n", pose.m_bone[i].x, by+pose.m_bone[i].y);
    }
}

void Display::draw()
{
    sf::RenderWindow win(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Pong" /* , sf::Style::Fullscreen */);

    sf::Image imgBack;
    sf::Texture txtBack;
    sf::Sprite sprBack;

    sf::RectangleShape midl(sf::Vector2f(6, WINDOW_HEIGHT));
    midl.setPosition(sf::Vector2f(WINDOW_WIDTH / 2., 0));
    midl.setFillColor(sf::Color(255, 255, 255, 100));
    centerized(midl);

    Ball b(sf::Vector2f(WINDOW_WIDTH / 2., WINDOW_HEIGHT / 2.), 400);
    BallView bv(b);
    b.setObserver(&bv);

    const double wthick = 5., decal = 60.;
    const double WALL_WIDTH = WINDOW_WIDTH - 2*decal,
                 WALL_HEIGHT = WINDOW_HEIGHT - 2*decal;
    Entity *walls[4];

    walls[0] = new Entity(sf::Vector2f(decal, decal),
                          sf::Vector2f(WALL_WIDTH, wthick));

    walls[1] = new Entity(sf::Vector2f(decal, decal),
                          sf::Vector2f(wthick, WALL_HEIGHT));

    walls[2] = new Entity(sf::Vector2f(decal, WINDOW_HEIGHT - wthick - decal),
                          sf::Vector2f(WALL_WIDTH, wthick));

    walls[3] = new Entity(sf::Vector2f(WINDOW_WIDTH - wthick - decal, decal),
                          sf::Vector2f(wthick, WALL_HEIGHT));

    WallView *wallsView[4];
    for(uint i = 0; i < 4; i++) {
        wallsView[i] = new WallView(*walls[i], sf::Color::Green, sf::Color(255, 127, 0));
        walls[i]->setObserver(wallsView[i]);
    }

    Pad *pad = Pads::get().create(AABB(sf::Vector2f(decal + wthick, decal + wthick),
                                       sf::Vector2f((WALL_WIDTH / 2.), WALL_HEIGHT - wthick)));
    PadView pv(*pad, 0);
    pad->setObserver(&pv);

    Pad *pad2 = Pads::get().create(AABB(sf::Vector2f(WINDOW_WIDTH / 2., decal + wthick),
                                        sf::Vector2f((WALL_WIDTH / 2.), WALL_HEIGHT - wthick)));
    PadView pv2(*pad2, 1);
    pad2->setObserver(&pv2);

    Goal goal(sf::Vector2f(decal + wthick - 5, WINDOW_HEIGHT / 2 - 100), 1);
    GoalView goalV(goal);
    goal.setObserver(&goalV);

    Goal goal2(sf::Vector2f(WINDOW_WIDTH - decal - wthick - 5, WINDOW_HEIGHT / 2 - 100), 0);
    GoalView goalV2(goal2);
    goal2.setObserver(&goalV2);

    BonusView bonV(Bonus::get(),
                   sf::Vector2f(WINDOW_WIDTH / 2., WINDOW_HEIGHT - 30),
                   sf::Vector2f(WINDOW_WIDTH / 2., WINDOW_HEIGHT /2.));
    Bonus::get().setObserver(&bonV);
    Bonus::get().reset();

    sf::Clock clock;
    double totalTmp = 0;
    int poseId = 0;

    Pose poseChinois("hands-up.pos");
    float poseEps = 0.64;
    int nbPose = 0;
    bool chinois = false;

    while(win.isOpen()) {
        /* controleur */
        sf::Vector2f posS(sf::Mouse::getPosition(win).x,
                          sf::Mouse::getPosition(win).y);
        // pad->setCenterMove(posS, clamp(tmp * 10, 0., 1.));
        // pad2->setCenterMove(posS + sf::Vector2f(WALL_WIDTH / 2., 0), clamp(tmp * 10, 0., 1.));
        posS.x /= scaleVect.x;
        posS.y /= scaleVect.y;


        Detection::get().computeNextPos();


        Obstacle::currentLvl =
            ((Detection::get().getDepthPlayer(1) +
              Detection::get().getDepthPlayer(2)) / 2.) *
            (MAX_LVL / (double) m_maxDepthLevel);


        sf::Event e;
        while(win.pollEvent(e)) {
            if(e.type == sf::Event::Closed ||
               (e.type == sf::Event::KeyPressed &&
                e.key.code == sf::Keyboard::Escape))
                win.close();

            if (e.type == sf::Event::KeyPressed) {
                switch (e.key.code) {
                case sf::Keyboard::F:
                    if (Detection::get().getPlayerNb() > 0) {
                        poseChinois = Detection::get().getPose(1);
                        printf("> set pose j1\n");
                    }
                    break;
                case sf::Keyboard::S:
                    if (Detection::get().getPlayerNb() > 0) {
                        char buf[255];
                        snprintf(buf, sizeof(buf), "pos%02d", poseId++);
                        poseChinois.save(buf);
                        printf("> wrote pose to %s\n", buf);
                    }
                    break;
                case sf::Keyboard::Up:
                    poseEps += 0.001;
                    printf("> eps %f\n", poseEps);
                    break;
                case sf::Keyboard::Down:
                    poseEps -= 0.001;
                    printf("> eps %f\n", poseEps);
                    break;
                default:
                    break;
                }
            }
        }


        // if (Detection::get().getPlayerNb() >= 1) {
        //     int id = 1;
        //     if (Detection::get().getPose(id).ressemble(poseChinois, poseEps)) {
        //         printf("> player %d fais le chinois! %d\n", id, nbPose);
        //         nbPose++;
        //         chinois = true;
        //     } else if (chinois) {
        //         printf("> player %d arrete\n", id);
        //         chinois = false;
        //         nbPose = 0;
        //     }
        // }

        double tmp = clock.restart().asSeconds();
        totalTmp += tmp;

        {
            sf::Vector2f pos(Detection::get().getPlayer(1).x * scaleVect.x, Detection::get().getPlayer(1).y * scaleVect.y);
            pad->setCenterMove(pos, clamp(tmp * 10, 0., 1.));
        }

        {
            sf::Vector2f pos(Detection::get().getPlayer(2).x * scaleVect.x, Detection::get().getPlayer(2).y * scaleVect.y);
            pad2->setCenterMove(pos, clamp(tmp * 10, 0., 1.));
        }


        /* vue */
        win.clear(sf::Color::Black);

        // image fond
        // depth_pixel_t *tab = Detection::get().getDepth();
        // unsigned char tabTestImage[Detection::get().getWidth() * Detection::get().getHeight() * 4];
        // for(int i = 0; i < Detection::get().getWidth() * Detection::get().getHeight(); i++) {
            // const uint MIN = 50, MAX = 255;
            // const unsigned char value = MIN + ((((double) tab[i]) / (Detection::get().getDepthMax()) * (MAX - MIN)));
            // tabTestImage[i * 4] = value;
            // tabTestImage[(i * 4) + 1] = value;
            // tabTestImage[(i * 4) + 2] = value;
            // tabTestImage[(i * 4) + 3] = 255;

            // test depth
       //      double t = m_maxDepthLevel * Obstacle::currentLvl / MAX_LVL;
       //      tabTestImage[i*4] = (tab[i] > t) ? 255 : 0;
       //      tabTestImage[(i*4)+1] = (tab[i] > t) ? 255 : 0;
       //      tabTestImage[(i*4)+2] = (tab[i] > t) ? 255 : 0;
       //      tabTestImage[(i*4)+3] = 255;
       // }

        // image fond
        rgb_pixel_t *tab = Detection::get().getRGB();
        unsigned char tabTestImage[Detection::get().getWidth() * Detection::get().getHeight() * 4];
        for(int i = 0; i < Detection::get().getWidth() * Detection::get().getHeight(); i++) {
            tabTestImage[i * 4] = tab[i][0];
            tabTestImage[(i * 4) + 1] = tab[i][1];
            tabTestImage[(i * 4) + 2] = tab[i][2];
            tabTestImage[(i * 4) + 3] = 100;
        }

        imgBack.create(Detection::get().getWidth(), Detection::get().getHeight(), tabTestImage);
        txtBack.loadFromImage(imgBack);
        sprBack.setTexture(txtBack);
        scaleVect.x = WINDOW_WIDTH / ((double) Detection::get().getWidth());
        scaleVect.y = WINDOW_HEIGHT / ((double) Detection::get().getHeight());
        sprBack.setScale(scaleVect);


        win.draw(sprBack);
        // fin image fond

        Score::get().draw(win);

        win.setView(Camera::get().getView());
        bv.draw(win);
        for(uint i = 0; i < 4; i++) {
            wallsView[i]->draw(win);
        }
        for(uint i = 0; i < m_ovs[Obstacle::currentLvl].size(); i++) {
//            m_ovs[Obstacle::currentLvl][i]->draw(win);
        }
        goalV.draw(win);
        goalV2.draw(win);
        win.draw(midl);
        pv.draw(win);
        pv2.draw(win);
        Particules::get().draw(win);
        bonV.draw(win, Detection::get());


        //printf("\n\n");
        win.display();

        /* modl */
        b.update(tmp);
        for(uint i = 0; i < Pads::get().size(); i++) {
            Pads::get().get(i)->update(tmp);
        }
        Camera::get().update();
        Bonus::get().update(tmp);
    }

    for(uint i = 0; i < 4; i++) {
        delete walls[i];
        delete wallsView[i];
    }
}
