#include "Score.hpp"
#include "constant.hpp"

Score *Score::instance(0);

Score::Score()
{
    m_value[0] = m_value[1] = 0;

    m_text[0].setString("0");
    m_text[0].setColor(getColor(0));
    m_text[0].setPosition(sf::Vector2f(WINDOW_WIDTH / 2. - 50, 20));
    m_text[0].setFont(getFont());
    txtCenterized(m_text[0]);

    m_text[1].setString("0");
    m_text[1].setColor(getColor(1));
    m_text[1].setPosition(sf::Vector2f(WINDOW_WIDTH / 2. + 50, 20));
    m_text[1].setFont(getFont());
    txtCenterized(m_text[1]);
}

uint Score::getScore(uint id) const
{
    return m_value[id];
}

void Score::add(uint id, uint s)
{
    m_value[id] += s;
    m_text[id].setString(nts(m_value[id]));
    Particules::get().add(30, m_text[id].getPosition() + m_text[id].getOrigin(),
                          sf::Vector2f(5, 2), 0, 360,
                          getColor(id), 300, 200);
    Camera::get().shake(50);
}

void Score::draw(sf::RenderTarget &rt)
{
    rt.draw(m_text[0]);
    rt.draw(m_text[1]);
}

Score &Score::get()
{
    if(instance == 0) {
        instance = new Score();
    }

    return *instance;
}

sf::Color Score::getColor(uint id)
{
    if(id == 0)
        return sf::Color(0, 0, 200);
    else
        return sf::Color(200, 0, 0);
}
