# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/Students/p0908952/pong/local/lib

BIN  = pong
DBIN = .
DOBJ = .obj
DSRC = src
DDEP = .dep
DEPC = $(shell pwd)

SRC = $(wildcard $(DSRC)/*.cpp)
OBJ = $(SRC:$(DSRC)%.cpp=$(DOBJ)%.o)
DEP = $(SRC:$(DSRC)%.cpp=$(DDEP)%.d)

CXXFLAGS = -I$(DEPC)/local/include -Wall -ffast-math -I../Common -I../../Include -I/Shared/OpenNI2/Include -I/Shared/NiTE2/Include -I. -ggdb3 #-O3
SFMLFLAGS =  -L$(DEPC)/local/lib -L$(DEPC)/local/lib64 -lsfml-graphics -lsfml-window -lsfml-audio -lsfml-system
LDFLAGS  = -lm -L/Shared/OpenNI2/Redist -L/Shared/NiTE2/Redist -lOpenNI2 -lNiTE2 -lGL -lGL -lGLEW -Wl,-rpath ./ $(SFMLFLAGS)

default: $(DBIN)/$(BIN)

-include $(DEP)

$(DDEP)/%.d: $(DSRC)/%.cpp
	@mkdir -p $(DDEP)
	@g++ $(CXXFLAGS) -MM -MT '$(patsubst $(DSRC)/%.cpp,$(DOBJ)/%.o,$<)' $< -MF $@

$(DOBJ)/%.o: $(DSRC)/%.cpp
	@echo CC $<
	@mkdir -p $(DOBJ)
	@g++ $(CXXFLAGS) -c $< -o $@ $(SFMLFLAGS)

$(DBIN)/$(BIN): $(OBJ)
	@echo
	@echo "  CXXFLAGS " $(CXXFLAGS)
	@echo "  LDFLAGS  " $(LDFLAGS)
	@echo
	@echo LD $@
	@mkdir -p $(DBIN)
	@g++ -o $@ $^ $(LDFLAGS)

clean:
	rm -rf $(DOBJ)/*
	rm -rf $(DDEP)/*

mrproper: clean
	rm -rf $(DBIN)/$(BIN)

.PHONY: clean mrproper default
